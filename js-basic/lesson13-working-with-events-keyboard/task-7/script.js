/**
 * За допомогою стрілок передвигати виділений
 * квадрат по клітинкам
 * При натисканні на пробіл випадковим чином переміщувати
 * активний квадрат
 *
 * ADVANCED:
 *
 * - направляти пакмена в сторону руху
 * Наприклад, якщо натиснули стрілку вверх, то смайл повинен бути
 * направлений вверх
 *
 * - дати користувачу можливість визначати розмір сітки
 * Наприклад, користувач вводить 10 і формується сітка 10х10,
 * випадково розміщується активний квадрат, а вся попередня
 * логіка щодо пересування зберігається
 * P.S. для цього потрібно очистити в html елемент board та генерувати
 * його повністю в js
 *
 */

let boardEl = document.querySelector('.board');
let blockEl = document.querySelector('.cell');
let leftMove = 0;
let topMove = 0;

document.addEventListener('keydown', event => {
    if (event.key == 'ArrowRight') {
        if (leftMove < 480) {
            addClassDirection('directionRight', 'directionLeft', 'directionTop', 'directionBottom');
            leftMove += 120;
            blockEl.style.left = leftMove + 'px'; 
        }
    } else if (event.key == 'ArrowLeft') {
        if (leftMove > 0) {
            addClassDirection('directionLeft', 'directionRight', 'directionTop', 'directionBottom');
            leftMove -= 120;
            blockEl.style.left = leftMove + 'px'; 
        }
    } else if (event.key == 'ArrowUp') {
        if (topMove > 0) {
            addClassDirection('directionTop', 'directionBottom', 'directionRight', 'directionLeft');
            topMove -= 120;
            blockEl.style.top = topMove + 'px'; 
        }      
    } else if (event.key == 'ArrowDown') {
        if (topMove < 480) {
            addClassDirection('directionBottom', 'directionTop', 'directionRight', 'directionLeft');
            topMove += 120;
            blockEl.style.top = topMove + 'px'; 
        }   
    } 
});

function addClassDirection (direction1, direction2, direction3, direction4) {
    blockEl.classList.add(direction1);
    blockEl.classList.remove(direction2);
    blockEl.classList.remove(direction3);
    blockEl.classList.remove(direction4);
}

