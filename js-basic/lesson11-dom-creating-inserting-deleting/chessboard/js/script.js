/**
 * Написати функцію createChessBoard, якій можна передавати розмір дошки,
 * а в результаті на сторінці утворюється шахматна дошка розміром 8х8
 */
function createChessBoard(size) {
	const board = document.createElement('div');
	const square = document.createElement('div');
	document.body.prepend(board);
	board.style.width = size + 'px';
	board.style.height = size + 'px';
	board.style.margin = '0 auto';
	document.body.style.backgroundColor = 'grey';
	
	function whiteSquare() {
		for (let i = 1; i <= 8; i++) {
		square[i] = document.createElement('div');
		board.append(square[i]);
		square[i].style.display = 'inline-block';
		if (i % 2 == 0) {
			square[i].style.backgroundColor = '#b58863';
		} else {
			square[i].style.backgroundColor = '#f0d9b5';
		}
		square[i].style.width = size / 8 + 'px';
		square[i].style.height = size / 8 + 'px';
		square[i].style.marginBottom = '-4px';
		}
	};

	function brownSquare() {
		for (let i = 1; i <= 8; i++) {
		square[i] = document.createElement('div');
		board.append(square[i]);
		square[i].style.display = 'inline-block';
		if (i % 2 == 0) {
			square[i].style.backgroundColor = '#f0d9b5';
		} else {
			square[i].style.backgroundColor = '#b58863';
		};
		square[i].style.width = size / 8 + 'px';
		square[i].style.height = size / 8 + 'px';
		square[i].style.marginBottom = '-4px';
		};
	};

	whiteSquare();
	brownSquare();
	whiteSquare();
	brownSquare();
	whiteSquare();
	brownSquare();
	whiteSquare();
	brownSquare();
};

createChessBoard(prompt('Введіть розмір шахматної дошки:'));
