/**
 * Заданий список студентів
 */
let students = [
    { name: 'John', gender: 'male', birthday: '1997-7-4' },
    { name: 'Sarah', gender: 'female', birthday: '1992-8-17' },
    { name: 'Sam', gender: 'male', birthday: '1987-4-12' },
    { name: 'Stephen', gender: 'male', birthday: '2001-8-1' },
    { name: 'Natalie', gender: 'female', birthday: '1993-07-20' },
    { name: 'Brad', gender: 'male', birthday: '1985-7-10' },
    { name: 'Amelia', gender: 'female', birthday: '1968-1-11' },
    { name: 'Roberto', gender: 'male', birthday: '1985-5-7' },
    { name: 'Patrick', gender: 'male', birthday: '1978-7-1' },
    { name: 'Antonio', gender: 'male', birthday: '1971-2-11' },
]
// /**
//  * ADVANCED:
//  * Вивести в консоль список студентів від наймолодшого
//  * до найстаршого
//  */
let sortedStudents = [];
for (let i = 0; i < students.length; i++) {
    let someYear = students[i].birthday;
    let someYearConvert= new Date(someYear);
    let someYearValue = someYearConvert.getTime();
    students[i].birthday = someYearValue;
    sortedStudents.push(students[i]);
}
function sortByAge(arr) {
    return arr.sort((a, b) => b.birthday - a.birthday)
}

sortByAge(sortedStudents);
console.log('Список студентів від наймолодшого до найстаршого ---->', sortedStudents);

























// let arr = [
//      { name: "Вася", age: 25 },
//      { name: "Петя", age: 30 },
//      { name: "Маша", age: 28 }
// ]
// console.log(arr);

// function sortByAge(arr) {
//   arr.sort((a, b) => a.age - b.age);
// }

// sortByAge(arr);

// console.log(arr)