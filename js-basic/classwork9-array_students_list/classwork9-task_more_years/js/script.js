/**
 * Заданий список студентів
 */
let students = [
    { name: 'John', gender: 'male', birthday: '1997-7-4' },
    { name: 'Sarah', gender: 'female', birthday: '1992-8-17' },
    { name: 'Sam', gender: 'male', birthday: '1987-4-12' },
    { name: 'Stephen', gender: 'male', birthday: '2001-8-1' },
    { name: 'Natalie', gender: 'female', birthday: '1993-07-20' },
    { name: 'Brad', gender: 'male', birthday: '1985-7-10' },
    { name: 'Amelia', gender: 'female', birthday: '1968-1-11' },
    { name: 'Roberto', gender: 'male', birthday: '1985-5-7' },
    { name: 'Patrick', gender: 'male', birthday: '1978-7-1' },
    { name: 'Antonio', gender: 'male', birthday: '1971-2-11' },
]
/**
 * Вивести в консоль окремо списки студентів, яким
 * - більше 20 років
 * - більше 30 років
 * - більше 40 років
 *
 */

//----------> За допомогою циклу for<-----------
let moreTwentyYear = [];
let moreThirtyYear = [];
let moreFortyYear = [];
let currentYear = new Date();
let currentYearValue = currentYear.getFullYear();
function checkAgeInStudents(students, moreYearArray, age) {
    for (let i = 0; i < students.length; i++) {
        let someYear = students[i].birthday;
        let someYearToString = new Date(someYear);
        let someYearValue = someYearToString.getFullYear();
        if (currentYearValue - someYearValue > age) {
            moreYearArray.push(students[i])
        }   
    }
    return moreYearArray
}
let resultMoreTwentyYear = checkAgeInStudents(students, moreTwentyYear, 20);
let resultMoreThirtyYear = checkAgeInStudents(students, moreThirtyYear, 30);
let resultMoreFortyYear = checkAgeInStudents(students, moreFortyYear, 40);
console.log('Студенти, яким більше 20-ти років -->', resultMoreTwentyYear);
console.log('Студенти, яким більше 30-ти років -->', resultMoreThirtyYear);
console.log('Студенти, яким більше 40-ка років -->', resultMoreFortyYear);


//----------> За допомогою методу filter <-----------
// let moreUnknownYears = [];
// let moreTwentyYear = [];
// let moreThirtyYear = [];
// let moreFortyYear = [];
// let currentYear = new Date();
// let currentYearValue = currentYear.getFullYear();
// function checkAgeStudents(moreUnknownYears, age) {
//     moreUnknownYears = students.filter((student) => {
//         let someYear = new Date(student.birthday);
//         let someYearValue = someYear.getFullYear();
//         return currentYearValue - someYearValue > age
//     })
//     return moreUnknownYears
// }
// moreTwentyYear = checkAgeStudents(moreTwentyYear, 20);
// moreThirtyYear = checkAgeStudents(moreThirtyYear, 30);
// moreFortyYear = checkAgeStudents(moreFortyYear, 40);
// console.log('Студенти, яким більше 20-ти років -->', moreTwentyYear)
// console.log('Студенти, яким більше 30-ти років -->', moreThirtyYear)
// console.log('Студенти, яким більше 40-ка років -->',moreFortyYear)





