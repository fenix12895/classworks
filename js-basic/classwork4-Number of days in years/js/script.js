let firstYear = +prompt('Input first year');
let secondYear = +prompt('Input second year');
let amountOfDays = 0;
let notALeapYear = 365;
let leapYear = 366;

if (firstYear < secondYear) {
    for (let i = firstYear; i < secondYear; i++) {
        if (i % 4 === 0) {
            amountOfDays += leapYear 
        } else {
            amountOfDays += notALeapYear; 
        }
    };
} else {
    for (let i = secondYear; i < firstYear; i++) {
        if (i % 4 === 0) {
            amountOfDays += leapYear 
        } else {
            amountOfDays += notALeapYear; 
        }
    };
}
// Перевірка останньої цифри кількості днів. Якщо кількість днів закінчується на число 1 то виводимо у відповіді "день" інакше виводимо "днів". Аналогічно зі словом (минув/минуло).
let dayString;
let past;
let lastInteger = amountOfDays.toString().split('').pop();
if (lastInteger === '1') {
    dayString = 'день';
    past = 'минув'
} else {
    dayString = 'днів';
    past = 'минуло'
}

if (firstYear < secondYear) {
    alert(`Від ${firstYear}-го року до ${secondYear}-го року ${past} ${amountOfDays} ${dayString}.`);
} else {
    alert(`Від ${secondYear}-го року до ${firstYear}-го року ${past} ${amountOfDays} ${dayString}.`); 
}






