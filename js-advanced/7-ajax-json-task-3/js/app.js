const movieSearchEl = document.querySelector('#movie-search');
const btnEL = document.querySelector('#search');
const movieTitleEl = document.querySelector('.movie__title');
const movieYearEl = document.querySelector('.movie__year');
const movieGenreEl = document.querySelector('.movie__genre');
const moviePlotEl = document.querySelector('.movie__plot');
const movieWrittenEl = document.querySelector('.movie__written');
const movieDirectedEl = document.querySelector('.movie__directed');
const movieActorsEl = document.querySelector('.movie__actors');
const moviePosterFill = document.querySelector('.movie__poster--fill');
const moviePosterFeatured = document.querySelector('.movie__poster--featured');
const loaderOverlayEl = document.querySelector('.loader-overlay');


const movieDataEl = document.querySelector('.movie__data');
const movieRrrorEl = document.querySelector('.movie__error');

window.addEventListener('keydown', (event) => {
    if (event.code === 'Enter') {
        fetch(`http://www.omdbapi.com/?apikey=edcaf654&t=${movieSearchEl.value}`)
        .then(response => {
            return response.json()
        })
        .then(data => { 
            loaderOverlayEl.style.display = 'block'
            setTimeout(() => {
                loaderOverlayEl.style.display = 'none'
            }, 2000) 
            console.log(data)
            if (data.Response === 'True') {
                movieDataEl.style.display = 'flex';
                movieRrrorEl.style.display = 'none'
                movieTitleEl.textContent = data.Title
                movieYearEl.textContent = data.Year
                movieGenreEl.textContent = data.Genre
                moviePlotEl.textContent = data.Plot
                movieWrittenEl.innerHTML = `<strong >Written by:</strong> ${data.Writer}`
                movieDirectedEl.innerHTML = `<strong >Directed by:</strong> ${data.Director}`
                let dataActors = data.Actors.split(",");
                movieActorsEl.innerHTML = '';
                dataActors.forEach(item => movieActorsEl.innerHTML += `<li>${item}</li>`)
                moviePosterFill.innerHTML = `<img src="${data.Poster}">`
                moviePosterFeatured.innerHTML = `<img src="${data.Poster}">`
            } else if (data.Response === 'False') {
                movieDataEl.style.display = 'none';
                movieRrrorEl.style.display = 'block'
                movieRrrorEl.innerHTML = `<h2>Error, movie ${movieSearchEl.value} not found</h2>`
            }
        })
    }
})




   