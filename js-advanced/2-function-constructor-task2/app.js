/**
 * Наявний уже функціонал для одного складу (kyivWarehouse)
 * Але з розвитком компанії з'явилося ще 2 склади (lvivWarehouse та kharkivWarehouse)
 * відповідно з місткістю 50 та 40 та поки що без продуктів
 * 
 * Відповідно потрібно зберегти логіку, але додати можливість також
 * масштабувати її
 * Для цього необхідно переробити об'єкт в функцію-конструктор з методами
 * та створити відповідно 3 склади
 * 
 * 
 * - додати на склад Львову 7 телефонів, 10 планшетів
 * - додати на склад Харькова 10 телефонів та 6 ноутбуків
 * - вивести загальну кількість товарів по кожному складу в консоль
 * - вивести всі товари кожного складу в консоль
 * - дізнатися, якого товару найбільше у Львові
 * 
 * 
 ADVANCED:
 - створити окремо функцію getTotalQuantity, якій можна передати назву товару
 та отримати загальну кількість товарів по всіх складах
 - додати можливість переміщення товарів між складами
 для цього додати метод moveProduct(name, quantity, targetWarehouse)
 - перемістити 4 ноутбуки з Києва у Львів
 * 
 */

 function Warehouse(name, capacity, products = {}) {
    this.name = name;
    this.maxCapacity = capacity;
    this.products = products;
}

Warehouse.prototype = {
    getTotalQuantity: function () {
        let sum = 0;
        for (const product in this.products) {
            sum += this.products[product];
        }
        return sum;
    },
    getProductQuantity: function (productName) {
        let quantity = this.products[productName];
        if (typeof quantity === 'undefined') {
            return "Product not found";
        } else {
            return quantity;
        }
    },
    getMaxQuantity: function () {
        let max = 0;
        let maxProductName;

        for (const product in this.products) {

            if (this.products[product] > max) {
                max = this.products[product];
                maxProductName = product;
            }
        }
        return maxProductName;
    },
    changeProductQuantity: function (productName, quantity) {
        if (quantity < 0) {
            console.log("Error");
            return;
        }

        const currentProductQuantity = this.products[productName] || 0;
        let possibleNewTotalQuantity = this.getTotalQuantity() - currentProductQuantity + quantity;
        if (possibleNewTotalQuantity <= this.maxCapacity) {
            this.products[productName] = quantity;
        } else {
            console.log("Error Max Capacity.");
        }
    },
    moveProduct(name, quantity, targetWarehouse) {
        if (this.products[name] >= quantity) {
            this.products[name] -= quantity
            targetWarehouse.products[name] += quantity 
        } else {
            return console.log(`Error, ${this.name} does not have this quantity of products.`);     
        }
    }  
}

const kyivWarehouse = new Warehouse('Kyiv Warehouse', 30, {
    phones: 2,
    headphones: 4,
    notebooks: 10,
    tablets: 7,
})

const lvivWarehouse = new Warehouse('Lviv Warehouse', 50, {
    phones: 0,
    headphones: 0,
    notebooks: 0,
    tablets: 0,
});
const kharkivWarehouse = new Warehouse('Kharkiv Warehouse', 40, {
    phones: 0,
    headphones: 0,
    notebooks: 0,
    tablets: 0,
});


// додати на склад Львову 7 телефонів, 10 планшетів
lvivWarehouse.changeProductQuantity('phones', 7);
lvivWarehouse.changeProductQuantity('tablets', 10);


// додати на склад Харькова 10 телефонів та 6 ноутбуків
kharkivWarehouse.changeProductQuantity('phones', 10);
kharkivWarehouse.changeProductQuantity('notebooks', 6);

//  вивести загальну кількість товарів по кожному складу в консоль
console.log(kyivWarehouse.getTotalQuantity());
console.log(lvivWarehouse.getTotalQuantity());
console.log(kharkivWarehouse.getTotalQuantity());

//  вивести всі товари кожного складу в консоль
console.log(kyivWarehouse.products)
console.log(lvivWarehouse.products);
console.log(kharkivWarehouse.products);

// дізнатися, якого товару найбільше у Львові
console.log(lvivWarehouse.getMaxQuantity());

// ADVANCED:
//  - створити окремо функцію getTotalQuantity, якій можна передати назву товару та отримати загальну кількість товарів по всіх складах
function getTotalQuantity(product, ...args) {
    const getTotalQuantityAll = args
    const result = getTotalQuantityAll.reduce((acc, curr) => {
        return acc + curr.products[product]
    }, 0)
    return result
}
// Input product name
let productName = 'headphones'
const result = `The total number of ${productName} in all warehouses are ${getTotalQuantity(productName, kyivWarehouse, lvivWarehouse, kharkivWarehouse)}.`;
console.log(result);

// -додати можливість переміщення товарів між складами для цього додати метод moveProduct(name, quantity, targetWarehouse)
// -перемістити 4 ноутбуки з Києва у Львів
kyivWarehouse.moveProduct('notebooks', 4, lvivWarehouse);

console.log(kyivWarehouse);
console.log(lvivWarehouse);








