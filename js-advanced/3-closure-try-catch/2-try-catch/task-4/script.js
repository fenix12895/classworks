/**
 * Написати функцію валідації поштової пошти
 * - не менше 5 символів
 * - наявність @
 * - закінчується на .com
 * 
 * Прокидувати помилку для кожної валідації, якщо вона не виконується
 * Повертати true у функції, якщо валідація проходить
 * 
 */

function validateEmail(email) {
    if (email.length < 5) {
        throw new Error('Не менше 5 символів')
    } else if (!email.includes('@')) {
        throw new Error('Має бути символ @')
    }
    return true
}

try {
    validateEmail('@')
    console.log(validateEmail('ffffff@'));
} catch (err) {
    console.log('from catch ->');
    console.error(err);
}

