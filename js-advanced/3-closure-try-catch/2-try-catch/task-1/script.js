let jackPerson;
let jennyPerson;

function Person(name, age) {
    this.name = name;
    this.age = age;
}

Person.prototype.sayHi = function () {
    console.log(`Hello, I'm ${this.name}`);
}

jennyPerson = new Person('Jenny', 23);

try {
    jackPerson.sayHi();
} catch (err) {
    console.error(err);
}
jennyPerson.sayHi();

