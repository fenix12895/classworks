let position = 'backend';
// const firstName = 'Jack';


// function sayHiBackendDev(name) {
//     let position = 'backend';
//     console.log(`Hi, I'm ${name}, I work as ${position} developer`);
// }

// function sayHiFrontendDev(name) {
//     let position = 'frontend';
//     console.log(`Hi, I'm ${name}, I work as ${position} developer`);
// }

// function sayHiMarketing(name) {
//     let position = 'marketing';
//     console.log(`Hi, I'm ${name}, I work as ${position} developer`);
// }


function sayHi(position) {
    return function (name) {
        console.log(`Hi, I'm ${name}, I work as ${position} developer`);
    }
}

const sayHiSEO = sayHi('SEO');
const sayHiSMM = sayHi('SMM')
const sayHiBackend = sayHi('Backend');

// console.log(sayHi);
// console.log(sayHiSEO);

sayHiSEO('Marry');
sayHiSEO('Mike');

sayHiBackend('John');
sayHiBackend('Jack');

// sayHi

// sayHiFrontendDev('Jack');

// position = 'frontend';
// sayHiFrontendDev('Jack');

// sayHiBackendDev('John');

/**
 * Counters
 */
