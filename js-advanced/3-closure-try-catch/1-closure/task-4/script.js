/**
 * Відрефакторити код позбавившись від дублювання коду
 * 
 * Для цього створити функцію createZoom, яка буде приймати назву селектора
 * (selector) та значення зуму (zoomValue)
 * 
 * ADVANCED:
 * Після створення зуму дати можливість викликати цю функцію, щоб дізнатися її
 * параметри та елемент
 * 
 * Наприклад, 
 * const zoomOut = createZoom('#zoom-out', 0.8);
 * console.log(zoomOut()) // { zoom: 0.8,  }
 */


 //----------------------

// let counter = 0;

// const zoomOut = document.querySelector('#zoom-out');

// zoomOut.addEventListener('click', function () {
//     const fontSize = getComputedStyle(document.body).fontSize;
//     const fontSizeValue = parseInt(fontSize);
//     console.log(fontSizeValue);
//     document.body.style.fontSize = `${fontSizeValue * 0.8}px`;
// });


// const zoomIn1 = document.querySelector('#zoom-in-1');

// zoomIn1.addEventListener('click', function () {
//     const fontSize = getComputedStyle(document.body).fontSize;
//     const fontSizeValue = parseInt(fontSize);
//     console.log(fontSizeValue);
//     document.body.style.fontSize = `${fontSizeValue * 1.5}px`;
// });

// const zoomIn2 = document.querySelector('#zoom-in-2');

// zoomIn2.addEventListener('click', function () {
//     const fontSize = getComputedStyle(document.body).fontSize;
//     const fontSizeValue = parseInt(fontSize);
//     console.log(fontSizeValue);
//     document.body.style.fontSize = `${fontSizeValue * 2}px`;
// });




// Відрефакторити код позбавившись від дублювання коду
// Для цього створити функцію createZoom, яка буде приймати назву селектора
// (selector) та значення зуму (zoomValue)
const zoomOutEl = document.querySelector('#zoom-out');
const zoomIn1El = document.querySelector('#zoom-in-1');
const zoomIn2El = document.querySelector('#zoom-in-2');
function createZoom(selector, zoomValue) {
    return function() {
        selector.addEventListener('click', function () {
            const fontSize = getComputedStyle(document.body).fontSize;
            const fontSizeValue = parseInt(fontSize);
            document.body.style.fontSize = `${fontSizeValue * zoomValue}px`;
        });
        return {zoomValue}
    }
}
const zoomOut = createZoom(zoomOutEl, 0.8);
const zoomIn1 = createZoom(zoomIn1El, 1.5);
const zoomIn2 = createZoom(zoomIn2El, 2);
zoomOut();
zoomIn1();
zoomIn2();

// ADVANCED:
// Після створення зуму дати можливість викликати цю функцію, щоб дізнатися її
// параметри та елемент
// * 
// Наприклад, 
// const zoomOut = createZoom('#zoom-out', 0.8);
// console.log(zoomOut()) // { zoom: 0.8,  }

console.log(zoomOut()) // { zoom: 0.8,  }
console.log(zoomIn1()) // { zoom: 1.5,  }
console.log(zoomIn2()) // { zoom: 2,  }