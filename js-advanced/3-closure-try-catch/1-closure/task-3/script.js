/**
 * Рахувати кількість натискань на пробіл, ентер,
 * шифт та альт клавіші
 * Відображати результат на сторінці
 *
 * Створити функцію, яка приймає тільки
 * назву клавіші, натискання якої потрібно рахувати,
 * а сам лічильник знаходиться в замиканні цієї функції
 * (https://learn.javascript.ru/closure)
 * id елемента, куди відображати результат має назву
 * "KEY-counter"
 *
 */

// let counterEnter = 0;
// let counterSpace = 0;
// let counterShift = 0;

function createCounter(keyName) {
    let counter = 0
    return function(event) {
        if (event.code === keyName && event.keyCode === 13) {
            let enterCounter = document.querySelector('#enter-counter');
            counter +=1;
            enterCounter.textContent = counter;
        } else if (event.code === keyName) {
            let spaceCounter = document.querySelector('#space-counter');
            counter += 1;
            spaceCounter.textContent = counter; 
        }  
        if (event.key === keyName && event.keyCode === 16) {
            let shiftCounter = document.querySelector('#shift-counter');
            counter += 1;
            shiftCounter.textContent = counter;
        } 
    }
}

// const increaseEnter = createCounter('Enter');
// const increaseSpace = createCounter('Space');
// const increaseShift = createCounter('Shift');

window.addEventListener('keyup', createCounter('Enter'));
window.addEventListener('keyup', createCounter('Space'));
window.addEventListener('keyup', createCounter('Shift'));



// window.addEventListener('keyup', function (event) {
//     if (event.code === "Enter") {
//         let enterCounter = document.querySelector('#enter-counter');
//         counterEnter = counterEnter + 1;
//         enterCounter.textContent = counterEnter;
//     } else if (event.code === "Space") {
//         let spaceCounter = document.querySelector('#space-counter');
//         counterSpace = counterSpace + 1;
//         spaceCounter.textContent = counterSpace;
//     } else if (event.key === "Shift") {
//         let shiftCounter = document.querySelector('#shift-counter');
//         counterShift = counterShift + 1;
//         shiftCounter.textContent = counterShift;
//     }

// })
