/**
 * - змінити існуючу функцію createProduct і за допомогою
 * неї створити ще 3 функції: createIPhone, createSamsung, createXiaomi
 * - за допомогою створених функцій створити по 2 телефона кожного бренду
 * - до створюваних продуктів додати метод getProductName, 
 * який буде повертати повне ім'я продукту, наприклад iPhone 12 pro
 * - спробувати змінити brand у якомусь продукті
 * - вивести в консоль повне ім'я цього за допомогою методу getProductName
 * - додати метод buy, який буде приймати суму, якщо її достатньо для покупки, то
 * виводити в консоль 'Ви можете придбати BRAND MODEL', інакше 
 * 'Ви не можете придбати BRAND MODEL, вам не вистачає X грн'
 * - викликати метод buy з різними параметрами
 * 
 */

function createProduct(brand) {
    return function (model, price) {
        return { brand, model, price }
    }
}

// function createIPhone(model, price) {
//     return { brand, model, price };
// }

const createIPhone = createProduct('Apple');
const createSamsung = createProduct('Samsung');
const createXiaomi = createProduct('Xiaomi');

const iphone12 = createIPhone(12, 800);
const samsung20 = createSamsung('S20', 920);
const iphone5 = createIPhone(5, 100);
console.log(iphone12);
console.log(samsung20);
console.log(iphone5);

