/**
 * Створити лічильники кількості населення для 3х
 * країн: України, Німеччини та Польщі
 * Для всіх країн населення спочатку дорівнює 0
 *
 * Збільшити населення на 1_000_000 для
 * України - двічі
 * Німеччини - чотири рази
 * Польщі - один раз
 *
 * При кожному збільшенні виводити в консоль
 * 'Населення COUNTRY_NAME збільшилося на X і становить Y'
 *
 * ADVANCED:
 * - Кожну секунду збільшувати населення України на 100_000 та також
 * виводити в консоль 'Населення COUNTRY_NAME збільшилося на X і становить Y'
 *
 */

// let populationUkraine = 0;
// let populationGermany = 0;
// let populationPoland = 0;

// populationUkraine += 1_000_000;
// console.log(`Населення України збільшилося на 1000000 і становить ${populationUkraine}`);

// populationUkraine += 1_000_000;
// console.log(`Населення України збільшилося на 1000000 і становить ${populationUkraine}`);

// populationGermany += 1_000_000;
// console.log(`Населення Німеччина збільшилося на 1000000 і становить 1000000`);

// populationGermany += 1_000_000;
// console.log(`Населення Німеччина збільшилося на 1000000 і становить 2000000`);

// populationGermany += 1_000_000;
// console.log(`Населення Німеччина збільшилося на 1000000 і становить 3000000`);

// populationGermany += 1_000_000;
// console.log(`Населення Німеччина збільшилося на 1000000 і становить 4000000`);

// populationPoland += 1_000_000;
// console.log(`Населення Польща збільшилося на 1000000 і становить 1000000`);


function Country(name, population = 0) {
    this.name = name;
    this.population = population;
    this.increasePopulation = function (addedPopulation = 1_000_000) {
        this.population += addedPopulation;
        console.log(`Населення ${this.name} збільшилося на ${addedPopulation} і становить ${this.population}`);
    }
}

const ukraine = new Country('Україна');
const germany = new Country('Німеччина');
const poland = new Country('Польща', 4_000_000);

console.log(poland.population);
console.log(ukraine.population);

ukraine.increasePopulation();
ukraine.increasePopulation();

germany.increasePopulation();
germany.increasePopulation();
germany.increasePopulation();
germany.increasePopulation();

poland.increasePopulation();
poland.increasePopulation(400_000);

console.log({ ukraine, germany, poland });

function makeCountry(name, population = 0) {
    return function (addedPopulation = 1_000_000) {
        population += addedPopulation;
        console.log(`Населення ${name} збільшилося на ${addedPopulation} і становить ${population}`);
    }
}

const increasePopulationUkraine = makeCountry('Україна');
increasePopulationUkraine();
increasePopulationUkraine();
increasePopulationUkraine();

const increasePopulationPoland = makeCountry('Poland', 4_000_000);
increasePopulationPoland();