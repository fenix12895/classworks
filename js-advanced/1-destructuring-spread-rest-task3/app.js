// ADVANCED
// - написати функцію getContinentData, яка буде повертати суму всіх площ країн вказаного континенту та список 
// країн цього континенту у вигляді об'єкту { area: 10_324_532, countries: ['China', 'India'] }

const COUNTRIES = [
    {
        code: 'US',
        name: 'United States',
        capital: 'Washington',
        area: 9_629_091,
        continent: 'NA'
    },
    {
        code: 'DE',
        name: 'Germany',
        capital: 'Berlin',
        area: 3_570_210,
        continent: 'EU'
    },
    {
        code: 'DK',
        name: 'Denmark',
        capital: 'Copenhagen',
        area: 430_940,
        continent: 'EU'
    },
    {
        name: 'Ukraine',
        capital: 'Kyiv',
        area: 603_700,
        code: 'UA',
        continent: 'EU'
    },
    {
        code: 'CN',
        name: 'China',
        capital: 'Beijing',
        area: 9_596_960,
        continent: 'AS'
    },
    {
        code: 'GB',
        name: 'United Kingdom',
        capital: 'London',
        area: 244_820,
        continent: 'EU'
    },
    {
        code: 'IN',
        name: 'India',
        capital: 'New Delhi',
        area: 3_287_590,
        continent: 'AS'
    }
];

function getContinentData(continent) {
    const filterCountryByContinent = COUNTRIES.filter((item) => {
        if (item.continent === continent) {
            return item;
        }
    })
    const filterCountryByContinentToArray = Object.values({...filterCountryByContinent}).map(v => Object.values(v))
    
    const filterCountryByContinentToArrayModified = filterCountryByContinentToArray.map(item => {
        return  item.splice(1, 1); 
    })

    const countries = filterCountryByContinentToArrayModified.flat()
  
    const area = filterCountryByContinent.reduce((acc, curr) => {
        return acc + curr.area;
    }, 0)

    return {countries, area}
}
let result = getContinentData('AS');
console.log(result);