/**
 * Написати клас Modal, за допомогою якого створити 2 об'єкта
 * модальних вікон: loginModal та signUpModal
 *
 * * loginModal має наступні параметри:
 * id - 'login-modal'
 * text - 'Ви успішно увійшли'
 * classList - 'modal login-modal'
 *
 * signUpModal має наступні параметри:
 * id - 'sign-up-modal'
 * text - 'Реєстрація'
 * classList - 'modal sign-up-modal'
 *
 * Кожне модальне вікно обов'язково має наступні методи:
 * - render() - генерує html код модального вікна
 * - open() - показує модальне вікно
 * - close() - закриває модальне вікно
 *
 * - За допомогою методу redner() додати html код
 * модальних вікок в кінець body
 * - При натисканні на кнопку Login за допомогою методу openModal
 * відкривати модальне вікно loginModal
 * - При натисканні на кнопку Sign Up за допомогою методу openModal
 * відкривати модальне вікно signUpModal
 *
 */

class Modal {
    constructor(id, text, classList) {
        this.id = id;
        this.text = text;
        this.classList = classList;
    }
    render() {
        const modal = document.createElement('div');
        this.classList.forEach(item => modal.classList.add(item));
        const modalInner = `
            <div class="modal-content">
                ${this.text}
                <span class="close">X</span>
            </div>
            `;
        modal.insertAdjacentHTML('afterbegin', modalInner);
        return modal;
    }
    open() {
        const modalEL = document.querySelector(`.${this.classList[1]}`);
        modalEL.classList.add('active');
    }
    close() {
        const modalEL = document.querySelector(`.${this.classList[1]}`);
        modalEL.classList.remove('active'); 
    }
}

// Login
const loginModal = new Modal('login-modal', 'Ви успішно увійшли',  ['modal', 'login-modal']);
document.body.append(loginModal.render())
const loginBtnEl = document.querySelector('#login-btn');

loginBtnEl.addEventListener('click', event => {
    event.stopPropagation();
    loginModal.open();
})
const closeEl = document.querySelector('.close');
closeEl.addEventListener('click', event => {
    event.stopPropagation();
    loginModal.close();
})
window.addEventListener('click', event => {
   if (event.target.classList.contains('modal-content')) {
      return
   } else {
    loginModal.close();
   }
})

//Sign up
const signUpModal = new Modal('sign-up-modal', 'Реєстрація', ['modal', 'sign-up-modal']);
document.body.append(signUpModal.render())
const signUpBtnEl = document.querySelector('#sign-up-btn');
signUpBtnEl.addEventListener('click', event => {
    event.stopPropagation();
    signUpModal.open();
})
closeEl.addEventListener('click', event => {
    event.stopPropagation();
    signUpModal.close();
})
window.addEventListener('click', event => {
    if (event.target.classList.contains('modal-content')) {
       return
    } else {
        signUpModal.close();
    }
 })
